Game Parameter	ID	Name			Wwise Object Path	Notes
	55766211	Sidechain_SFX			\Sidechaining_01\Sidechain_SFX	

Audio Bus	ID	Name			Wwise Object Path	Notes
	393239870	SFX			\Default Work Unit\Master Audio Bus\SFX	
	805203703	Master Secondary Bus			\Default Work Unit\Master Secondary Bus	
	1383017243	DynosHit			\Default Work Unit\Master Audio Bus\SFX\DynosHit	
	2607556080	Menu			\Default Work Unit\Master Audio Bus\Menu	
	2981377429	Ambiance			\Default Work Unit\Master Audio Bus\SFX\Ambiance	
	3393986022	FireSystem			\Default Work Unit\Master Audio Bus\SFX\FireSystem	
	3803692087	Master Audio Bus			\Default Work Unit\Master Audio Bus	
	3991942870	Music			\Default Work Unit\Master Audio Bus\Music	

Effect plug-ins	ID	Name	Type				Notes
	938720393	Wwise Compressor (Custom)	Wwise Compressor			
	999560550	Wwise Compressor (Custom)	Wwise Compressor			
	1041102290	Wwise Meter (Custom)	Wwise Meter			

