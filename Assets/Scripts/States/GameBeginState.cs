﻿using System;
using Assets.scripts.States;
using UnityEngine;
using Assets.Scripts.Models.States;
using Assets.Scripts.Models;
using Assets.Scripts.Models.Tiles;

namespace Assets.Scripts.States
{
    public class GameBeginState : State<GameManager>
    {
        private static GameBeginState _instance;

        private GameModel _gameModel;
        private float _nextTick;
        private float _tickInterval = 25;

        public static GameBeginState Instance()
        {
            if (_instance == null)
                _instance = new GameBeginState();

            return _instance;
        }

        public override void Enter(GameManager entity)
        {
            _gameModel = entity.GameModel;
            _gameModel.Player.Show();
            _gameModel.DifficultyLevel = 1;
            _gameModel.LavaLevel = -1;
            _gameModel.Init();
            entity.Player.PosY = 0;
            entity.Player.PosX = 3;
            entity.GameModel.Score = 0;
            _gameModel.UpdateDifficultyLevel();

            AkSoundEngine.PostEvent("Level_0_Fire", entity.GameModel.Player.gameObject);
        }

        public override void Execute(GameManager entity)
        {
            if (Time.time >= _nextTick)
            {
                AkSoundEngine.PostEvent("DigDirt", entity.Player.gameObject);
                var explosion = GameObject.Instantiate(Resources.Load(
                    "DirtExplosion",
                    typeof(GameObject)),
                    new Vector3(entity.Player.transform.position.x, entity.Player.transform.position.y - 0.5f, -5),
                    Quaternion.identity) as GameObject;
                entity.Player.Animator.SetTrigger("DigDown");
                entity.GameModel.Player.UpdatePosition();
                GetCurrentTile().State = EmptyTileState.Instance();
                GetCurrentTile().Render();
                UpdatePlayerPosition(entity);
                CalculateNextTick();
            }
        }

        public override void Exit(GameManager entity)
        {
        }

        public override void FixedExecute(GameManager entity)
        {
        }

        private void UpdatePlayerPosition(GameManager gameManager)
        {
            if (gameManager.GameModel.Player.PosY == 5)
                gameManager.StateMachine.ChangeState(GameState.Instance());
            else
                gameManager.GameModel.Player.PosY += 1;
        }

        private void CalculateNextTick()
        {
            _nextTick = Time.time + _tickInterval * Time.deltaTime;
        }

        private Tile GetCurrentTile()
        {
            return _gameModel.TileMap[_gameModel.Player.PosY][_gameModel.Player.PosX];
        }
    }
}
