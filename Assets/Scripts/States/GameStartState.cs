﻿using Assets.scripts.States;
using Assets.Scripts.Models;
using UnityEngine;

namespace Assets.Scripts.States
{
    public class GameStartState : State<GameManager>
    {
        private static GameStartState _instance;

        public static GameStartState Instance()
        {
            if (_instance == null)
                _instance = new GameStartState();

            return _instance;
        }

        public override void Enter(GameManager entity)
        {
            entity.GameStartModel.Anim_GameStart.SetActive(true);
            entity.GameStartModel.EndOfAnim_Callback = () => entity.StateMachine.ChangeState(GameBeginState.Instance());
            entity.GameStartModel.InvokeScrollDown();
            AkSoundEngine.PostEvent("Game_Start", entity.GameModel.Player.gameObject);
        }

        public override void Execute(GameManager entity)
        {
            if (entity.GameStartModel.IsReadyToScrollDown)
            {
                var translationVector = new Vector3(0, 2.3f * Time.deltaTime, 0);
                entity.GameStartModel.Transform_GameStart.Translate(translationVector);
                entity.GameModel.Transform_GameBoard.Translate(translationVector);
            }
        }

        public override void Exit(GameManager entity)
        {
        }

        public override void FixedExecute(GameManager entity)
        {
        }
    }
}
