﻿using System;
using Assets.scripts.States;
using Assets.Scripts.Models.States;

namespace Assets.Scripts.States
{
    public class GameOverState : State<GameManager>
    {
        private static GameOverState _instance;

        public static GameOverState Instance()
        {
            if (_instance == null)
                _instance = new GameOverState();

            return _instance;
        }

        public override void Enter(GameManager entity)
        {
            entity.GameModel.TileMap[entity.Player.PosY][entity.Player.PosX].State = EmptyTileState.Instance();
            entity.GameModel.TileMap[entity.Player.PosY][entity.Player.PosX].Render();
            entity.GameOverModel.UI_GameOver.SetActive(true);
            entity.Player.Animator.SetTrigger("Die");
            AkSoundEngine.PostEvent("GameOver", entity.GameModel.Player.gameObject);
            entity.GameOverModel.OnClick_Restart = () => { entity.StateMachine.ChangeState(GameBeginState.Instance()); };
        }

        public override void Execute(GameManager entity)
        {
        }

        public override void Exit(GameManager entity)
        {
            entity.GameOverModel.UI_GameOver.SetActive(false);
            entity.Player.Animator.SetTrigger("Restart");
            AkSoundEngine.PostEvent("Level_Start", entity.GameModel.Player.gameObject);
        }

        public override void FixedExecute(GameManager entity)
        {
        }
    }
}
