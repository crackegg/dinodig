﻿using Assets.scripts.States;
using Assets.Scripts.Models.Tiles;
using UnityEngine;

namespace Assets.Scripts.States
{
    public class GameState : State<GameManager>
    {
        private static GameState _instance;

        private float _nextTick;
        private float _tickInterval = 25f;

        public static GameState Instance()
        {
            if (_instance == null)
                _instance = new GameState();

            return _instance;
        }

        public override void Enter(GameManager entity)
        {
            entity.GameModel.UI_Game.SetActive(true);
        }

        public override void Execute(GameManager entity)
        {
            if (entity.GameModel.LavaLevel == entity.Player.PosY)
            {
                entity.StateMachine.ChangeState(GameOverState.Instance());
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow) && entity.Player.PosX > 0)
            {
                var tile = entity.GameModel.TileMap[entity.Player.PosY][entity.Player.PosX - 1];
                var canMove = tile.CanMove();
                entity.Player.Animator.SetTrigger("DigLeft");
                
                if (canMove)
                {
                    entity.Player.PosX -= 1;
                    tile.DugEffect(entity.GameModel);
                    tile.Render();
                }
            }

            if (Input.GetKeyDown(KeyCode.RightArrow) && entity.Player.PosX < entity.GameModel.Width - 1)
            {
                var tile = entity.GameModel.TileMap[entity.Player.PosY][entity.Player.PosX + 1];
                var canMove = tile.CanMove();
                entity.Player.Animator.SetTrigger("DigRight");

                if (canMove)
                {
                    entity.Player.PosX += 1;
                    tile.DugEffect(entity.GameModel);
                    tile.Render();
                }
            }
        }

        public override void Exit(GameManager entity)
        {
            entity.GameModel.UI_Game.SetActive(false);
        }

        public override void FixedExecute(GameManager entity)
        {
            entity.Player.transform.localPosition = new Vector2(entity.Player.PosX, entity.Player.transform.localPosition.y);
            entity.GameModel.MoveTiles();

            if (Time.time >= _nextTick)
            {
                entity.GameModel.Tick();
                CalculateNextTick();
            }

            _tickInterval = 25f - (entity.GameModel.DifficultyLevel - 2);
            
        }

        private void CalculateNextTick()
        {
            _nextTick = Time.time + _tickInterval * Time.deltaTime;
        }
    }
}
