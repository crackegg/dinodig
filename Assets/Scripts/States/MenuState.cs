﻿using System;
using Assets.scripts.States;
using Assets.Scripts.Models;

namespace Assets.Scripts.States
{
    public class MenuState : State<GameManager>
    {
        private static MenuState _instance;

        private MenuModel _gameMenuModel;

        public static MenuState Instance()
        {
            if (_instance == null)
                _instance = new MenuState();

            return _instance;
        }

        public override void Enter(GameManager entity)
        {
            _gameMenuModel = entity.GameMenuModel;
            _gameMenuModel.UI_Menu.SetActive(true);
            _gameMenuModel.Anim_Menu.SetActive(true);
            _gameMenuModel.ClickOnStart = () =>
            {
                entity.StateMachine.ChangeState(GameStartState.Instance());
            };
        }

        public override void Execute(GameManager entity)
        {

        }

        public override void Exit(GameManager entity)
        {
            _gameMenuModel.UI_Menu.SetActive(false);
            _gameMenuModel.Anim_Menu.SetActive(false);
        }

        public override void FixedExecute(GameManager entity)
        {
        }
    }
}
