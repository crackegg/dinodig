﻿using System;
using Assets.Scripts.Models.Tiles;
using UnityEngine;

namespace Assets.Scripts.Models.States
{
    public class EggTileState : TileState
    {
        private static EggTileState _instance;
        private RuntimeAnimatorController _animatorController;

        public static EggTileState Instance()
        {
            if (_instance == null)
                _instance = new EggTileState();

            return _instance;
        }

        private EggTileState()
        {
            _animatorController = Resources.Load<RuntimeAnimatorController>("EggDirtController");
        }

        public override bool DugEffect(Tile tile, GameModel model)
        {
            tile.State = EmptyTileState.Instance();
            var point = GameObject.Instantiate(Resources.Load(
                "Point50", 
                typeof(GameObject)), 
                new Vector3(model.Player.transform.position.x, model.Player.transform.position.y, -5f), 
                Quaternion.identity);
            
            var explosion = GameObject.Instantiate(Resources.Load(
                "DirtExplosion",
                typeof(GameObject)),
                new Vector3(model.Player.transform.position.x, model.Player.transform.position.y - 0.5f, -5),
                Quaternion.identity) as GameObject;
            AkSoundEngine.PostEvent("DigDirtEgg", model.Player.gameObject);
            return true;
        }

        public override void Move(Tile tile)
        {
            tile.transform.Translate(new Vector2(0, Constants.MotionDuration * Time.deltaTime));
        }

        public override void Render(Tile tile)
        {
            tile.SpriteRenderer.sprite = null;
            tile.SpriteRenderer.sortingLayerName = "Tiles";

            tile.Animator.runtimeAnimatorController = _animatorController;
        }

        public override bool CanMove()
        {
            return true;
        }
    }
}
