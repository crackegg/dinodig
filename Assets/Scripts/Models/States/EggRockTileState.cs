﻿using Assets.Scripts.Models.Tiles;
using UnityEngine;

namespace Assets.Scripts.Models.States
{
    public class EggRockTileState : TileState
    {
        private static EggRockTileState _instance;
        private RuntimeAnimatorController _animatorController;

        public static EggRockTileState Instance()
        {
            if (_instance == null)
                _instance = new EggRockTileState();

            return _instance;
        }

        private EggRockTileState()
        {
            _animatorController = Resources.Load<RuntimeAnimatorController>("EggRockController");
        }

        public override bool DugEffect(Tile tile, GameModel model)
        {
            tile.State = CrackedEggRockTileState.Instance();
            AkSoundEngine.PostEvent("HitRock", model.Player.gameObject);
            return false;
        }

        public override void Move(Tile tile)
        {
            tile.transform.Translate(new Vector2(0, Constants.MotionDuration * Time.deltaTime));
        }

        public override void Render(Tile tile)
        {
            tile.SpriteRenderer.sprite = null;
            tile.SpriteRenderer.sortingLayerName = "Tiles";

            tile.Animator.runtimeAnimatorController = _animatorController;
        }

        public override bool CanMove()
        {
            return false;
        }
    }
}
