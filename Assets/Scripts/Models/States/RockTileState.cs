﻿using System;
using Assets.Scripts.Models.Tiles;
using UnityEngine;

namespace Assets.Scripts.Models.States
{
    public class RockTileState : TileState
    {
        private static RockTileState _instance;
        private Sprite _sprite;

        public static RockTileState Instance()
        {
            if (_instance == null)
                _instance = new RockTileState();

            return _instance;
        }

        public RockTileState()
        {
            _sprite = Resources.Load<Sprite>("RockTile");
        }

        public override bool DugEffect(Tile tile, GameModel model)
        {
            tile.State = CrackedRockTileState.Instance();
            AkSoundEngine.PostEvent("HitRock", model.Player.gameObject);
            return false;
        }

        public override void Move(Tile tile)
        {
            tile.transform.Translate(new Vector2(0, Constants.MotionDuration * Time.deltaTime));
        }

        public override void Render(Tile tile)
        {
            tile.Animator.Stop();
            tile.Animator.runtimeAnimatorController = null;

            tile.SpriteRenderer.sprite = _sprite;
            tile.SpriteRenderer.sortingLayerName = "Tiles";
        }

        public override bool CanMove()
        {
            return false;
        }
    }
}
