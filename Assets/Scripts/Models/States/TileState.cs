﻿using Assets.Scripts.Models.Tiles;

namespace Assets.Scripts.Models.States
{
    public abstract class TileState
    {
        public abstract bool DugEffect(Tile tile, GameModel model);
        public abstract void Render(Tile tile);
        public abstract void Move(Tile tile);
        public abstract bool CanMove();
    }
}
