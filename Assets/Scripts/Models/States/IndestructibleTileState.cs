﻿using Assets.Scripts.Models.Tiles;
using UnityEngine;

namespace Assets.Scripts.Models.States
{
    public class IndestructibleTileState : TileState
    {
        private static IndestructibleTileState _instance;
        private Sprite _sprite;

        public static IndestructibleTileState Instance()
        {
            if (_instance == null)
                _instance = new IndestructibleTileState();

            return _instance;
        }

        public IndestructibleTileState()
        {
            _sprite = Resources.Load<Sprite>("Rock_Gems");
        }

        public override bool DugEffect(Tile tile, GameModel model)
        {
            AkSoundEngine.PostEvent("HitRock", model.Player.gameObject);
            return false;
        }

        public override void Move(Tile tile)
        {
            tile.transform.Translate(new Vector2(0, Constants.MotionDuration * Time.deltaTime));
        }

        public override void Render(Tile tile)
        {
            tile.Animator.Stop();
            tile.Animator.runtimeAnimatorController = null;

            tile.SpriteRenderer.sprite = _sprite;
            tile.SpriteRenderer.sortingLayerName = "Tiles";
        }

        public override bool CanMove()
        {
            return false;
        }
    }
}
