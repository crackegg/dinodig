﻿using System;
using Assets.Scripts.Models.Tiles;
using UnityEngine;

namespace Assets.Scripts.Models.States
{
    public class LavaTileState : TileState
    {
        private static LavaTileState _instance;
        private RuntimeAnimatorController _animatorController;

        public static LavaTileState Instance()
        {
            if (_instance == null)
                _instance = new LavaTileState();

            return _instance;
        }

        private LavaTileState()
        {
            _animatorController = Resources.Load<RuntimeAnimatorController>("LavaController");
        }

        public override bool DugEffect(Tile tile, GameModel model)
        {
            return false;
        }

        public override void Render(Tile tile)
        {
            tile.SpriteRenderer.sprite = null;

            tile.Animator.runtimeAnimatorController = _animatorController;
            tile.SpriteRenderer.sortingLayerName = "Lava";
        }

        public override void Move(Tile tile)
        {
            tile.transform.Translate(new Vector2(0, -0.2f * Time.deltaTime));
        }

        public override bool CanMove()
        {
            return false;
        }
    }
}
