﻿using System;
using Assets.Scripts.Models.Tiles;
using UnityEngine;

namespace Assets.Scripts.Models.States
{
    public class IceTileState : TileState
    {
        private static IceTileState _instance;
        private RuntimeAnimatorController _animatorController;

        public static IceTileState Instance()
        {
            if (_instance == null)
                _instance = new IceTileState();

            return _instance;
        }

        public IceTileState()
        {
            _animatorController = Resources.Load<RuntimeAnimatorController>("IceController");
        }

        public override bool DugEffect(Tile tile, GameModel model)
        {
            tile.State = EmptyTileState.Instance();
            var explosion = GameObject.Instantiate(Resources.Load(
               "IceExplosion",
               typeof(GameObject)),
               new Vector3(model.Player.transform.position.x, model.Player.transform.position.y - 0.5f, -5),
               Quaternion.identity) as GameObject;
            AkSoundEngine.PostEvent("HitIce", model.Player.gameObject);
            if (model.LavaLevel >= 0)
                model.LavaLevel -= 1;

            return true;
        }

        public override void Move(Tile tile)
        {
            tile.transform.Translate(new Vector2(0, Constants.MotionDuration * Time.deltaTime));
        }

        public override void Render(Tile tile)
        {
            tile.SpriteRenderer.sprite = null;
            tile.SpriteRenderer.sortingLayerName = "Tiles";
            tile.Animator.runtimeAnimatorController = _animatorController;
        }

        public override bool CanMove()
        {
            return true;
        }
    }
}
