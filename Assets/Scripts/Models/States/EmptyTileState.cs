﻿using System;
using Assets.Scripts.Models.Tiles;
using UnityEngine;

namespace Assets.Scripts.Models.States
{
    public class EmptyTileState : TileState
    {
        private static EmptyTileState _instance;

        public static EmptyTileState Instance()
        {
            if (_instance == null)
                _instance = new EmptyTileState();

            return _instance;
        }

        public override bool CanMove()
        {
            return true;
        }

        public override bool DugEffect(Tile tile, GameModel model)
        {
            return true;
        }

        public override void Move(Tile tile)
        {
            tile.transform.Translate(new Vector2(0, Constants.MotionDuration * Time.deltaTime));
        }

        public override void Render(Tile tile)
        {
            tile.Animator.Stop();
            tile.Animator.runtimeAnimatorController = null;

            tile.SpriteRenderer.sprite = null;
            tile.SpriteRenderer.sortingLayerName = "Tiles";
        }
    }
}
