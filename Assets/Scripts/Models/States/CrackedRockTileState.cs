﻿using System;
using Assets.Scripts.Models.Tiles;
using UnityEngine;

namespace Assets.Scripts.Models.States
{
    public class CrackedRockTileState : TileState
    {
        private static CrackedRockTileState _instance;
        private Sprite _sprite;

        public static CrackedRockTileState Instance()
        {
            if (_instance == null)
                _instance = new CrackedRockTileState();

            return _instance;
        }

        public CrackedRockTileState()
        {
            _sprite = Resources.Load<Sprite>("CrackedRockTile");
        }

        public override bool DugEffect(Tile tile, GameModel model)
        {
            tile.State = EmptyTileState.Instance();
            var explosion = GameObject.Instantiate(Resources.Load(
               "RockExplosion",
               typeof(GameObject)),
               new Vector3(model.Player.transform.position.x, model.Player.transform.position.y - 0.5f, -5),
               Quaternion.identity) as GameObject;
            AkSoundEngine.PostEvent("DestroyRock", model.Player.gameObject);
            return true;
        }

        public override void Move(Tile tile)
        {
            tile.transform.Translate(new Vector2(0, Constants.MotionDuration * Time.deltaTime));
        }

        public override void Render(Tile tile)
        {
            tile.Animator.Stop();
            tile.Animator.runtimeAnimatorController = null;

            tile.SpriteRenderer.sprite = _sprite;
            tile.SpriteRenderer.sortingLayerName = "Tiles";
        }

        public override bool CanMove()
        {
            return true;
        }
    }
}
