﻿using Assets.Scripts.Models.Tiles;
using UnityEngine;

namespace Assets.Scripts.Models.States
{
    public class CrackedEggRockTileState : TileState
    {
        private static CrackedEggRockTileState _instance;
        private RuntimeAnimatorController _animatorController;

        public static CrackedEggRockTileState Instance()
        {
            if (_instance == null)
                _instance = new CrackedEggRockTileState();

            return _instance;
        }

        public CrackedEggRockTileState()
        {
            _animatorController = Resources.Load<RuntimeAnimatorController>("CrackedEggRockController");
        }

        public override bool DugEffect(Tile tile, GameModel model)
        {
            var point = GameObject.Instantiate(Resources.Load(
                "Point100",
                typeof(GameObject)),
                new Vector3(model.Player.transform.position.x, model.Player.transform.position.y, -5f),
                Quaternion.identity);
            tile.State = EmptyTileState.Instance();
            var explosion = GameObject.Instantiate(Resources.Load(
               "RockExplosion",
               typeof(GameObject)),
               new Vector3(model.Player.transform.position.x, model.Player.transform.position.y - 0.5f, -5),
               Quaternion.identity) as GameObject;
            AkSoundEngine.PostEvent("DestroyRockEgg", model.Player.gameObject);

            return true;
        }

        public override void Move(Tile tile)
        {
            tile.transform.Translate(new Vector2(0, Constants.MotionDuration * Time.deltaTime));
        }

        public override void Render(Tile tile)
        {
            tile.SpriteRenderer.sprite = null;
            tile.SpriteRenderer.sortingLayerName = "Tiles";

            tile.Animator.runtimeAnimatorController = _animatorController;
        }

        public override bool CanMove()
        {
            return true;
        }
    }
}
