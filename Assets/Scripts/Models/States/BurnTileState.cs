﻿using System;
using Assets.Scripts.Models.Tiles;
using UnityEngine;

namespace Assets.Scripts.Models.States
{
    public class BurnTileState : TileState
    {
        private static BurnTileState _instance;
        private Sprite _sprite;

        public static BurnTileState Instance()
        {
            if (_instance == null)
                _instance = new BurnTileState();

            return _instance;
        }

        private BurnTileState()
        {
            _sprite = Resources.Load<Sprite>("BurnTile");
        }

        public override bool DugEffect(Tile tile, GameModel model)
        {
            return false;
        }

        public override void Render(Tile tile)
        {
            tile.Animator.Stop();
            tile.Animator.runtimeAnimatorController = null;

            tile.SpriteRenderer.sprite = _sprite;
            tile.SpriteRenderer.sortingLayerName = "Lava";            
        }

        public override void Move(Tile tile)
        {
            tile.transform.Translate(new Vector2(0, -0.2f * Time.deltaTime));
        }

        public override bool CanMove()
        {
            return false;
        }
    }
}
