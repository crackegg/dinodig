﻿using Assets.Scripts.Models.States;
using Assets.Scripts.Models.Tiles;
using UnityEngine;

namespace Assets.Scripts.Models
{
    public class GameModel : MonoBehaviour
    {
        private ITilesPatterns _currentTilePatterns;

        public GameObject GameBoard;
        public GameObject UI_Game;
        public Player Player;

        public Transform Transform_TilesBoard;
        public Transform Transform_Background;
        public Transform Transform_GameBoard;
        
        public ushort Width;
        public ushort Height;
        public short LavaLevel = -1;
        public ushort DifficultyLevel = 1;
        public ulong Score = 0;

        public Tile[][] TileMap;
        public Tile[] NextTileRow;
        
        public GameObject[][] Background;

        public void Awake()
        {
            InitBackground();
            InitTiles();
            _currentTilePatterns = TilePatterns.GetLevelPatternCollection(DifficultyLevel);
        }

        public void MoveTiles()
        {
            for (int j = 0; j < Height - 1; j++)
            {
                for (int i = 0; i < Width; i++)
                {
                    TileMap[j][i].Move();
                }
            }

            Player.transform.Translate(new Vector2(0, Constants.MotionDuration * Time.deltaTime));
        }

        public void Tick()
        {
            var canMove = GetTileUnder(Player.PosX, Player.PosY).DugEffect(this);
            
            UpdateTileMap(canMove);
            Player.UpdatePosition();
            Player.Animator.SetTrigger("DigDown");
            Score += 200;
            UpdateDifficultyLevel();
        }

        public void UpdateTileMap(bool canMove)
        {
            if (!canMove)
            {
                LavaLevel += 1;
                AkSoundEngine.PostEvent("Level_" + LavaLevel + "_Fire", Player.gameObject);

                for (int j = (LavaLevel + 1); j < Height - 1; j++)
                {
                    for (int i = 0; i < Width; i++)
                    {
                        TileMap[j][i].transform.localPosition = new Vector2(i, -j);
                        TileMap[j][i].Render();
                    }
                }
            }
            else
            {
                for (int j = (LavaLevel + 1); j < Height - 1; j++)
                {
                    for (int i = 0; i < Width; i++)
                    {
                        TileMap[j][i].State = TileMap[j + 1][i].State;
                        TileMap[j][i].transform.localPosition = new Vector2(i, -j);
                        TileMap[j][i].Render();
                    }
                }

                var randomPattern = _currentTilePatterns.GetPattern(Random.Range(1, 101));
                for (int i = 0; i < Width; i++)
                {
                    TileMap[Height - 1][i].State = randomPattern[i];
                }
            }

            if (LavaLevel > -1)
            {
                UpdateBurnTiles();
                UpdateLavaTiles();
            }
        }

        private void UpdateBurnTiles()
        {
            for (int j = 0; j <= LavaLevel; j++)
            {
                for (int i = 0; i < Width; i++)
                {
                    TileMap[j][i].State = BurnTileState.Instance();
                    TileMap[j][i].transform.localPosition = new Vector2(i, -j);
                    TileMap[j][i].Render();
                }
            }
        }

        private void UpdateLavaTiles()
        {
            for (int i = 0; i < Width; i++)
            {
                TileMap[LavaLevel][i].State = LavaTileState.Instance();
                TileMap[LavaLevel][i].transform.localPosition = new Vector2(i, -LavaLevel);
                TileMap[LavaLevel][i].Render();
            }
        }

        private Tile GetTileUnder(ushort x, ushort y)
        {
            return TileMap[y + 1][x];
        }

        public void Init()
        {
            for (int j = 0; j < Height; j++)
            {
                for (int i = 0; i < Width; i++)
                {
                    TileMap[j][i].State = DirtTileState.Instance();
                    TileMap[j][i].Render();
                }
            }
        }

        private void InitBackground()
        {
            Background = new GameObject[Height][];
            for (int j = 0; j < Height; j++)
            {
                Background[j] = new GameObject[Width];
                for (int i = 0; i < Width; i++)
                {
                    var tile = Instantiate(Resources.Load("BackgroundTile", typeof(GameObject))) as GameObject;
                    tile.transform.parent = Transform_Background;
                    tile.transform.localPosition = new Vector2(i - 0.2f, -j);
                    tile.transform.localScale = new Vector2(0.275f, 0.275f);

                    Background[j][i] = tile;
                }
            }
        }

        private void InitTiles()
        {
            TileMap = new Tile[Height][];
            for (int j = 0; j < Height; j++)
            {
                TileMap[j] = new Tile[Width];
                for (int i = 0; i < Width; i++)
                {
                    var tile = Instantiate(Resources.Load("Tile", typeof(Tile))) as Tile;
                    tile.transform.parent = Transform_TilesBoard;
                    tile.transform.localPosition = new Vector2(i, -j);
                    tile.transform.localScale = new Vector2(0.192f, 0.192f);
                    tile.State = DirtTileState.Instance();

                    TileMap[j][i] = tile;
                }
            }
        }

        public void UpdateDifficultyLevel()
        {
            if (Score <= Constants.Level2Score)
            {
                _currentTilePatterns = TilePatterns.GetLevelPatternCollection(1);
            }
            else if (Score > Constants.Level2Score && Score <= Constants.Level3Score)
            {
                
                _currentTilePatterns = TilePatterns.GetLevelPatternCollection(2);
                if (DifficultyLevel == 1)
                {
                    AkSoundEngine.PostEvent("Level02", Player.gameObject);
                    for (int i = 0; i < Width; i++)
                    {
                        TileMap[Height - 3][i].State = IceTileState.Instance();
                        TileMap[Height - 2][i].State = RockTileState.Instance();
                        TileMap[Height - 1][i].State = IceTileState.Instance();
                    }
                }
                DifficultyLevel = 2;
            }
            else if (Score > Constants.Level3Score && Score <= Constants.Level4Score)
            {
                _currentTilePatterns = TilePatterns.GetLevelPatternCollection(3);
                if (DifficultyLevel == 2)
                {
                    AkSoundEngine.PostEvent("Level03", Player.gameObject);
                    for (int i = 0; i < Width; i++)
                    {
                        TileMap[Height - 3][i].State = IceTileState.Instance();
                        TileMap[Height - 2][i].State = RockTileState.Instance();
                        TileMap[Height - 1][i].State = DirtTileState.Instance();
                    }
                    TileMap[Height - 2][2].State = EggRockTileState.Instance();
                    TileMap[Height - 2][3].State = EggRockTileState.Instance();
                    TileMap[Height - 2][4].State = EggRockTileState.Instance();
                    TileMap[Height - 1][3].State = IceTileState.Instance();
                }
                DifficultyLevel = 3;
            }
            else if (Score > Constants.Level4Score && Score <= Constants.Level5Score)
            {
                _currentTilePatterns = TilePatterns.GetLevelPatternCollection(4);
                if (DifficultyLevel == 3)
                    AkSoundEngine.PostEvent("Level04", Player.gameObject);

                DifficultyLevel = 4;
            }
            else if (Score > Constants.Level5Score && Score <= Constants.Level6Score)
            {
                _currentTilePatterns = TilePatterns.GetLevelPatternCollection(5);
                if (DifficultyLevel == 4)
                {
                    AkSoundEngine.PostEvent("Level05", Player.gameObject);
                    for (int i = 0; i < Width; i++)
                    {
                        TileMap[Height - 3][i].State = IceTileState.Instance();
                        TileMap[Height - 2][i].State = IndestructibleTileState.Instance();
                        TileMap[Height - 1][i].State = RockTileState.Instance();
                    }
                    TileMap[Height - 2][3].State = EggRockTileState.Instance();
                    TileMap[Height - 1][2].State = DirtTileState.Instance();
                    TileMap[Height - 1][4].State = IceTileState.Instance();
                    TileMap[Height - 1][3].State = DirtTileState.Instance();
                }
                DifficultyLevel = 5;
            }
            else
            {
                _currentTilePatterns = TilePatterns.GetLevelPatternCollection(6);

                if (DifficultyLevel == 5)
                    AkSoundEngine.PostEvent("Level06", Player.gameObject);

                DifficultyLevel = 6;
            }
            
        }
    }
}
