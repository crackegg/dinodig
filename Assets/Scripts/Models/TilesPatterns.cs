﻿using Assets.Scripts.Models.States;
using Assets.Scripts.Patterns;
using System;

namespace Assets.Scripts.Models
{
    public interface ITilesPatterns
    {
        TileState[] GetPattern(int rn);
    }

    public class TilePatterns
    {
        public static ITilesPatterns GetLevelPatternCollection(int level)
        {
            switch (level)
            {
                case 1:
                    return new Level1Patterns();
                case 2:
                    return new Level2Patterns();
                case 3:
                    return new Level3Patterns();
                case 4:
                    return new Level4Patterns();
                case 5:
                    return new Level5Patterns();
                case 6:
                    return new Level6Patterns();
            }

            throw new Exception("TilePatterns - Level must be between 1 and 10");
        }
    }
}
