﻿using System;
using UnityEngine;

namespace Assets.Scripts.Models
{
    public class GameStartModel : MonoBehaviour
    {
        public GameObject Anim_GameStart { get { return gameObject; } }
        public Transform Transform_GameStart { get { return transform; } }
        public Action EndOfAnim_Callback;

        public bool IsReadyToScrollDown;

        public void Callback_GameStartEnd()
        {
            if (EndOfAnim_Callback == null)
                throw new Exception("EndOfAnim_Callback is undefined");

            EndOfAnim_Callback();
        }

        public void InvokeScrollDown()
        {
            Invoke("ScrollDown", 2f);
        }

        public void ScrollDown()
        {
            IsReadyToScrollDown = true;
        }
    }
}
