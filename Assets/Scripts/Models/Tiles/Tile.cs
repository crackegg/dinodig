﻿using Assets.Scripts.Models.States;
using UnityEngine;

namespace Assets.Scripts.Models.Tiles
{
    public class Tile : MonoBehaviour
    {
        public TileState State;
        public Animator Animator;
        public SpriteRenderer SpriteRenderer;

        public bool DugEffect(GameModel model)
        {
            return State.DugEffect(this, model);
        }

        public void Render()
        {
            State.Render(this);
        }
        
        public void Move()
        {
            State.Move(this);
        }

        public bool CanMove()
        {
            return State.CanMove();
        }
    }
}
