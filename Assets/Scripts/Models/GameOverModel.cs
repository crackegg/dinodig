﻿using System;
using UnityEngine;

namespace Assets.Scripts.Models
{
    public class GameOverModel : MonoBehaviour
    {
        public GameObject UI_GameOver;

        public Action OnClick_Restart;

        public void Restart()
        {
            OnClick_Restart();
        }
    }
}
