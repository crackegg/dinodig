﻿using System;
using UnityEngine;

namespace Assets.Scripts.Models
{
    public class MenuModel : MonoBehaviour
    {
        public GameObject UI_Menu;
        public GameObject Anim_Menu;

        public Action ClickOnStart;

        public void OnClick_Start()
        {
            if (ClickOnStart == null)
                throw new Exception("ClickOnStart is undefined");

            ClickOnStart();
        }
    }
}
