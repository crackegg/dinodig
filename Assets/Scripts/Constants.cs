﻿namespace Assets.Scripts
{
    public static class Constants
    {
        public const float MotionDuration = 1f;

        public const ulong Level2Score = 3500;
        public const ulong Level3Score = 7500;
        public const ulong Level4Score = 15000;
        public const ulong Level5Score = 25000;
        public const ulong Level6Score = 50000;
    }
}
