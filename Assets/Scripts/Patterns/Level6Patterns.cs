﻿using Assets.Scripts.Models;
using Assets.Scripts.Models.States;
using System;

namespace Assets.Scripts.Patterns
{
    public class Level6Patterns : ITilesPatterns
    {
        public TileState[] GetPattern(int rn)
        {
            if (rn >= 1 && rn <= 10)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    EggTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    EggTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 11 && rn <= 20)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 21 && rn <= 30)
            {
                return new TileState[]
                {
                    EggRockTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    EggTileState.Instance(),
                    RockTileState.Instance(),
                    IndestructibleTileState.Instance(),
                };
            }
            else if (rn >= 31 && rn <= 40)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                };
            }
            else if (rn >= 41 && rn <= 50)
            {
                return new TileState[]
                {
                    EggRockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    IndestructibleTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 51 && rn <= 60)
            {
                return new TileState[]
                {
                    IceTileState.Instance(),
                    EggRockTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    EggTileState.Instance(),
                    EggTileState.Instance(),
                };
            }
            else if (rn >= 61 && rn <= 70)
            {
                return new TileState[]
                {
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    EggTileState.Instance(),
                    IndestructibleTileState.Instance(),
                    IndestructibleTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 71 && rn <= 80)
            {
                return new TileState[]
                {
                    IceTileState.Instance(),
                    RockTileState.Instance(),
                    EggTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    EggRockTileState.Instance(),
                };
            }
            else if (rn >= 81 && rn <= 90)
            {
                return new TileState[]
                {
                    IndestructibleTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    EggTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 91 && rn <= 100)
            {
                return new TileState[]
                {
                    EggRockTileState.Instance(),
                    IndestructibleTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    EggTileState.Instance(),
                };
            }

            throw new Exception("Level6Patterns - Number not between 1 and 100");
        }
    }
}
