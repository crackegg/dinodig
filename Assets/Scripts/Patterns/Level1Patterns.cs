﻿using Assets.Scripts.Models;
using Assets.Scripts.Models.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Patterns
{
    public class Level1Patterns : ITilesPatterns
    {
        public TileState[] GetPattern(int rn)
        {
            if (rn >= 1 && rn <= 35)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 36 && rn <= 51)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 52 && rn <= 56)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                };
            }
            else if (rn >= 57 && rn <= 59)
            {
                return new TileState[]
                {
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    IceTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                };
            }
            else if (rn >= 60 && rn <= 67)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 68 && rn <= 72)
            {
                return new TileState[]
                {
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                };
            }
            else if (rn >= 73 && rn <= 78)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 79 && rn <= 84)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 85 && rn <= 92)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    IceTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 93 && rn <= 100)
            {
                return new TileState[]
                {
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    IceTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                };
            }

            throw new Exception("Level1Patterns - Number not between 1 and 100");
        }
    }
}
