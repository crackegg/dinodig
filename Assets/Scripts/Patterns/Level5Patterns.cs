﻿using Assets.Scripts.Models;
using Assets.Scripts.Models.States;
using System;

namespace Assets.Scripts.Patterns
{
    public class Level5Patterns : ITilesPatterns
    {
        public TileState[] GetPattern(int rn)
        {
            if (rn >= 1 && rn <= 15)
            {
                return new TileState[]
                {
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                };
            }
            else if (rn >= 16 && rn <= 25)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    EggTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 26 && rn <= 36)
            {
                return new TileState[]
                {
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    EggRockTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    EggTileState.Instance(),
                };
            }
            else if (rn >= 37 && rn <= 44)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    IceTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    EggTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 45 && rn <= 51)
            {
                return new TileState[]
                {
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    EggTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                };
            }
            else if (rn >= 52 && rn <= 57)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    IceTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                };
            }
            else if (rn >= 58 && rn <= 63)
            {
                return new TileState[]
                {
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 64 && rn <= 77)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    IndestructibleTileState.Instance(),
                    IndestructibleTileState.Instance(),
                    DirtTileState.Instance(),
                    IceTileState.Instance(),
                    DirtTileState.Instance(),
                    EggRockTileState.Instance(),
                };
            }
            else if (rn >= 78 && rn <= 86)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                    EggTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 87 && rn <= 100)
            {
                return new TileState[]
                {
                    IndestructibleTileState.Instance(),
                    RockTileState.Instance(),
                    EggRockTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }

            throw new Exception("Level5Patterns - Number not between 1 and 100");
        }
    }
}
