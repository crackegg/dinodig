﻿using Assets.Scripts.Models;
using Assets.Scripts.Models.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Patterns
{
    class Level2Patterns : ITilesPatterns
    {
        public TileState[] GetPattern(int rn)
        {
            if (rn >= 1 && rn <= 30)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 31 && rn <= 43)
            {
                return new TileState[]
                {
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                };
            }
            else if (rn >= 44 && rn <= 50)
            {
                return new TileState[]
                {
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    IceTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 51 && rn <= 55)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    EggTileState.Instance(),
                    DirtTileState.Instance(),
                    IceTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 56 && rn <= 59)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    IceTileState.Instance(),
                    RockTileState.Instance(),
                    EggTileState.Instance(),
                };
            }
            else if (rn >= 60 && rn <= 67)
            {
                return new TileState[]
                {
                    IceTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    IceTileState.Instance(),
                };
            }
            else if (rn >= 68 && rn <= 74)
            {
                return new TileState[]
                {
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 75 && rn <= 85)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    IceTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 86 && rn <= 93)
            {
                return new TileState[]
                {
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    EggTileState.Instance(),
                    RockTileState.Instance(),
                    IceTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                };
            }
            else if (rn >= 94 && rn <= 100)
            {
                return new TileState[]
                {
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    IceTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }

            throw new Exception("Level2Patterns - Number not between 1 and 100");
        }
    }
}
