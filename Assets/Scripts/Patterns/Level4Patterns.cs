﻿using Assets.Scripts.Models;
using Assets.Scripts.Models.States;
using System;

namespace Assets.Scripts.Patterns
{
    public class Level4Patterns : ITilesPatterns
    {
        public TileState[] GetPattern(int rn)
        {
            if (rn >= 1 && rn <= 20)
            {
                return new TileState[]
                {
                    EggRockTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                };
            }
            else if (rn >= 21 && rn <= 34)
            {
                return new TileState[]
                {
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    EggRockTileState.Instance(),
                    DirtTileState.Instance(),
                    EggTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                };
            }
            else if (rn >= 35 && rn <= 41)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    EggRockTileState.Instance(),
                    EggRockTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 42 && rn <= 49)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    EggTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 50 && rn <= 55)
            {
                return new TileState[]
                {
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    IceTileState.Instance(),
                    RockTileState.Instance(),
                };
            }
            else if (rn >= 56 && rn <= 61)
            {
                return new TileState[]
                {
                    EggRockTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    EggRockTileState.Instance(),
                    RockTileState.Instance(),
                };
            }
            else if (rn >= 64 && rn <= 67)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                    EggRockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 78 && rn <= 81)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                    EggTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 82 && rn <= 89)
            {
                return new TileState[]
                {
                    RockTileState.Instance(),
                    IceTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 90 && rn <= 100)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    EggRockTileState.Instance(),
                };
            }

            throw new Exception("Level4Patterns - Number not between 1 and 100");
        }
    }
}
