﻿using Assets.Scripts.Models;
using Assets.Scripts.Models.States;
using System;

namespace Assets.Scripts.Patterns
{
    public class Level3Patterns : ITilesPatterns
    {
        public TileState[] GetPattern(int rn)
        {
            if (rn >= 1 && rn <= 25)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 26 && rn <= 39)
            {
                return new TileState[]
                {
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                };
            }
            else if (rn >= 40 && rn <= 45)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    EggTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 46 && rn <= 52)
            {
                return new TileState[]
                {
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                    IceTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                };
            }
            else if (rn >= 53 && rn <= 57)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 57 && rn <= 63)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    EggRockTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 64 && rn <= 71)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    EggRockTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    EggTileState.Instance(),
                    DirtTileState.Instance(),
                };
            }
            else if (rn >= 72 && rn <= 84)
            {
                return new TileState[]
                {
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    DirtTileState.Instance(),
                    EggRockTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    IceTileState.Instance(),
                };
            }
            else if (rn >= 85 && rn <= 91)
            {
                return new TileState[]
                {
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    EggTileState.Instance(),
                    DirtTileState.Instance(),
                    RockTileState.Instance(),
                };
            }
            else if (rn >= 92 && rn <= 100)
            {
                return new TileState[]
                {
                    IceTileState.Instance(),
                    RockTileState.Instance(),
                    EggRockTileState.Instance(),
                    RockTileState.Instance(),
                    RockTileState.Instance(),
                    DirtTileState.Instance(),
                    EggTileState.Instance(),
                };
            }

            throw new Exception("Level3Patterns - Number not between 1 and 100");
        }
    }
}
