﻿using Assets.Scripts.Models;
using Assets.Scripts.States;
using UnityEngine;

namespace Assets.Scripts
{
    public class GameManager : MonoBehaviour
    {
        public StateMachine<GameManager> StateMachine;
        public GameStartModel GameStartModel;
        public MenuModel GameMenuModel;
        public GameModel GameModel;
        public GameOverModel GameOverModel;

        public Player Player { get { return GameModel.Player; } }

        public void Awake()
        {
            StateMachine = new StateMachine<GameManager>(this);
            StateMachine.Init(MenuState.Instance());
        }
        
        public void Update()
        {
            StateMachine.Update();
        }

        public void FixedUpdate()
        {
            StateMachine.FixedUpdate();
        }
    }
}
