﻿using UnityEngine;

namespace Assets
{
    public class PlayerView : MonoBehaviour
    {
        //[Inject]
        //public Player Player { get; set; }
        ////[Inject]
        ////public GameManager GameManager { get; set; }
        //private float _timestamp;
        //private float _offset = 0.001f;
        //private float _inputTimeStamp;
        //private SpriteRenderer[,] _sprites;
        //public RuntimeAnimatorController LavaAnimatorController;
        //public RuntimeAnimatorController IceAnimatorController;
        //public RuntimeAnimatorController EggDirtAC;
        //public RuntimeAnimatorController EggRockAC;
        //public RuntimeAnimatorController EggRockBrokenAC;
        //public Sprite Rock1;
        //public Sprite BrokenRock1;
        //public Sprite Rock2;
        //public Sprite BrokenRock2;
        //public Sprite Sediment;
        //public Sprite Empty;
        //public Sprite Burnt;
        //public Sprite Ice;
        //public Sprite Egg;
        //public Sprite Lava;
        //public GameObject ButtonRestart;
        //public Text ScoreText;
        //public GameObject ScoreObject;

        //public GameObject GameOver;
        //public GameObject GameLevel;
        //public GameObject Act2;

        //public List<GameObject> DirtTiles { get; set; }

        //public bool _isDiggingLeft = true;

        //private Animator _animator;

        //private bool _isAwake = false;
        //private bool _isDeath = false;

        //protected override void Awake()
        //{
        //    _animator = GetComponent<Animator>();
        //    _sprites = new SpriteRenderer[7, 9];
        //    DirtTiles = new List<GameObject>();
        //    Init();
        //}

        //public void Init()
        //{
        //    DirtTiles.ForEach(x => Destroy(x));
        //    for (var i = 0; i < 9; i++)
        //    {
        //        for (var j = 0; j < 7; j++)
        //        {
        //            var dirt = Instantiate(Resources.Load("Slot_dirt", typeof(GameObject)), new Vector3(j+1.74f, (9 - i) - 0.3f), Quaternion.identity) as GameObject;
        //            var gameObject = Instantiate(Resources.Load("Slot", typeof(GameObject)), new Vector3(j+2, 9 - i), Quaternion.identity) as GameObject;
        //            DirtTiles.Add(dirt);
        //            DirtTiles.Add(gameObject);
        //            gameObject.transform.SetParent(GameLevel.transform);
        //            dirt.transform.SetParent(GameLevel.transform);
        //            _sprites[j, i] = gameObject.GetComponent<SpriteRenderer>();
        //        }
        //    }
        //}

        //public void RestartGame()
        //{
        //    Player.PosX = 3;
        //    Player.PosY = 10;
        //    transform.position = new Vector3(Player.PosX+2, Player.Y);
        //    ScoreObject.SetActive(true);
        //    GameOver.SetActive(false);
        //    ButtonRestart.SetActive(false);
        //    ScoreText.gameObject.SetActive(false);
        //    GameManager.CanStartAnimation = true;
        //    _isDeath = false;
        //    Init();
        //    _animator.SetTrigger("Restart");
        //    GameManager.Init();
        //}

        //public void Reset()
        //{
        //    for (var i = 0; i < 9; i++)
        //    {
        //        for (var j = 0; j < 7; j++)
        //        {
        //            GameManager.Sprites[j, i].gameObject.transform.position = new Vector3(j + 2, 9 - i);
        //        }
        //    }
        //    transform.position = new Vector3(transform.position.x, Player.Y);
        //}

        //public void Render()
        //{
        //    for (var i = 0; i < 9; i++)
        //    {
        //        for (var j = 0; j < 7; j++)
        //        {
        //            var position = GameManager.Sprites[j, i].gameObject.transform.position;
        //            GameManager.Sprites[j, i].gameObject.transform.position = new Vector3(position.x, position.y - _offset);
        //            GameManager.Sprites[j, i].sprite = GetSprite(GameManager.State[(i * 7) + j]);
        //            var animator = GameManager.Sprites[j, i].gameObject.GetComponent<Animator>();
        //            if (GameManager.State[(i * 7) + j] == 8)
        //            {
        //                GameManager.Sprites[j, i].gameObject.transform.position = new Vector3(position.x, position.y - _offset * 3);
        //                animator.runtimeAnimatorController = LavaAnimatorController;
        //            }
        //            else if (GameManager.State[(i * 7) + j] == 6)
        //            {
        //                GameManager.Sprites[j, i].gameObject.transform.position = new Vector3(position.x , position.y - _offset);
        //                animator.runtimeAnimatorController = IceAnimatorController;
        //            }
        //            else if (GameManager.State[(i * 7) + j] == 3)
        //            {
        //                GameManager.Sprites[j, i].gameObject.transform.position = new Vector3(position.x, position.y - _offset * 3);
        //                animator.runtimeAnimatorController = null;
        //            }
        //            else if (GameManager.State[(i * 7) + j] == 11)
        //            {
        //                GameManager.Sprites[j, i].gameObject.transform.position = new Vector3(position.x, position.y - _offset);
        //                animator.runtimeAnimatorController = EggDirtAC;
        //            }
        //            else if (GameManager.State[(i * 7) + j] == 12)
        //            {
        //                GameManager.Sprites[j, i].gameObject.transform.position = new Vector3(position.x, position.y - _offset);
        //                animator.runtimeAnimatorController = EggRockAC;
        //            }
        //            else if (GameManager.State[(i * 7) + j] == 13)
        //            {
        //                GameManager.Sprites[j, i].gameObject.transform.position = new Vector3(position.x, position.y - _offset);
        //                animator.runtimeAnimatorController = EggRockBrokenAC;
        //            }
        //            else
        //            {
        //                GameManager.Sprites[j, i].gameObject.transform.position = new Vector3(position.x, position.y - _offset);
        //                animator.runtimeAnimatorController = null;
        //            }
        //        }
        //    }
        //    transform.position = new Vector3(transform.position.x, transform.position.y - _offset);
        //}

        //public Sprite GetSprite(ushort index)
        //{
        //    switch (index)
        //    {
        //        case 0:
        //            return Sediment;
        //        case 1:
        //            return Rock1;
        //        case 2:
        //            return Empty;
        //        case 3:
        //            return Burnt;
        //        case 4:
        //            return Rock2;
        //        case 6:
        //            return Ice;
        //        case 8:
        //            return Lava;
        //        case 9:
        //            return BrokenRock1;
        //        case 10:
        //            return BrokenRock2;
        //        default:
        //            return Empty;
        //    }
        //}

        //public void Show50Points()
        //{
        //    var a = GameObject.Instantiate(Resources.Load("Point50", typeof(GameObject)), new Vector3(Player.X + 2.5f, Player.Y), Quaternion.identity);
        //}

        //public void Show100Points()
        //{
        //    var a = GameObject.Instantiate(Resources.Load("Point100", typeof(GameObject)), new Vector3(Player.X + 2.5f, Player.Y), Quaternion.identity);
        //}

        //public void Update()
        //{
        //    if (!_isAwake)
        //    {
        //        GameManager.Sprites = _sprites;
        //        GameManager.Sediment = Sediment;
        //        GameManager.Empty = Empty;
        //        GameManager.Show50Points = Show50Points;
        //        GameManager.Show100Points = Show100Points;
        //        _isAwake = true;
        //    }

        //    if (GameManager.IsPrologue) {
        //        if (Player.Y <= 5)
        //        {
        //            GameManager.IsPrologue = false;
        //            Act2.SetActive(false);
        //        }

        //        if (GameManager.CanStartAnimation)
        //        {
        //            if (Time.time >= _timestamp)
        //            {
        //                Player.Y -= 1;
        //                transform.position = new Vector3(Player.X+2, Player.Y);
        //                GameManager.State[(9 - Player.Y) * 7 + Player.X] = 2;
        //                GameManager.Sprites[Player.X, (9 - Player.Y)].sprite = GetSprite(GameManager.State[(9 - Player.Y) * 7 + Player.X]);
        //                _animator.SetTrigger("DigDown");
        //                _timestamp = Time.time + GameManager.Difficulty;
        //            }
        //        }
        //        return;
        //    }

        //    if (GameManager.FireLevel == 5)
        //    {
        //        if (!_isDeath)
        //        {
        //            DeathStinger.Play();
        //            GameOverSong.Play();
        //            MainSong.Stop();
        //            _animator.SetTrigger("Die");
        //            Sizzling.Play();
        //            _isDeath = true;
        //        }

        //        ScoreObject.SetActive(false);
        //        GameOver.SetActive(true);
        //        ButtonRestart.SetActive(true);
        //        ScoreText.text = string.Format("YOUR SCORE: {0}", GameManager.Score);
        //        ScoreText.gameObject.SetActive(true);
        //        return;
        //    }

        //    if (Time.time >= _inputTimeStamp)
        //    {
        //        if (Input.GetKeyDown("left"))
        //        {
        //            if (Player.X > 0)
        //            {
        //                Player.X -= 1;
        //                var tile = GameManager.State[(9 - Player.Y) * 7 + Player.X];
        //                if (tile != 1 && tile != 4 && tile != 12)
        //                {
        //                    if (tile == 11)
        //                    {
        //                        Show50Points();
        //                    }

        //                    HitSound(tile);
        //                    UpdateInputTimeStamp();
        //                    _isDiggingLeft = true;
        //                    _animator.SetTrigger("DigLeft");
        //                    transform.position = new Vector3(transform.position.x - 1, transform.position.y);
        //                }
        //                else
        //                {
        //                    Player.X += 1;
        //                }

        //                SetPlayerState();
        //            }
        //        }
        //        else if (Input.GetKeyDown("right"))
        //        {
        //            if (Player.X < 6)
        //            {

        //                Player.X += 1;
        //                var tile = GameManager.State[(9 - Player.Y) * 7 + Player.X];
        //                if (tile != 1 && tile != 4 && tile != 12)
        //                {
        //                    if (tile == 11)
        //                    {
        //                        Show50Points();
        //                    }

        //                    HitSound(tile);
        //                    UpdateInputTimeStamp();
        //                    _isDiggingLeft = false;
        //                    _animator.SetTrigger("DigRight");
        //                    transform.position = new Vector3(transform.position.x + 1, transform.position.y);
        //                }
        //                else
        //                {
        //                    Player.X -= 1;
        //                }

        //                SetPlayerState();
        //            }
        //        }

        //    }

        //    if (Time.time >= _timestamp)
        //    {
        //        GameManager.Tick();
        //        Reset();
        //        _animator.SetTrigger("DigDown");
        //        _timestamp = Time.time + GameManager.Difficulty;
        //    }

        //    Render();
        //}

        //private void UpdateInputTimeStamp()
        //{
        //    _inputTimeStamp = Time.time + 0.15f;
        //}

        //public void PlayHitSound()
        //{
        //    var tileUnder = GameManager.State[(10 - Player.Y) * 7 + Player.X];
        //    HitSound(tileUnder);
        //}

        //private void HitSound(ushort tile)
        //{
        //    if (tile == 0)
        //    {
        //        HitDirt.Play();
        //        var i = GameObject.Instantiate(Resources.Load("DirtExplosion", typeof(GameObject)), new Vector3(Player.X + 2.5f, Player.Y, -5), Quaternion.identity);
        //    }
        //    else if (tile == 1 || tile == 12)
        //    {
        //        HitRock.Play();
        //        var i = GameObject.Instantiate(Resources.Load("RockExplosion", typeof(GameObject)), new Vector3(Player.X + 2.5f, Player.Y, -5), Quaternion.identity);
        //    }
        //    else if(tile == 4)
        //    {
        //        HitRock.Play();
        //        var i = GameObject.Instantiate(Resources.Load("CrystalExplosion", typeof(GameObject)), new Vector3(Player.X + 2.5f, Player.Y, -5), Quaternion.identity);
        //    }
        //    else if (tile == 6)
        //    {
        //        HitIce.Play();
        //        var i = GameObject.Instantiate(Resources.Load("IceExplosion", typeof(GameObject)), new Vector3(Player.X + 2.5f, Player.Y, -5), Quaternion.identity);
        //    }
        //    else if (tile == 9 || tile == 10)
        //    {
        //        HitDestroy.Play();
        //    } else if (tile == 11)
        //    {
        //        HitEgg.Play();
        //    }else if(tile == 13)
        //    {
        //        EggRockDestroy.Play();
        //    }
        //}

        //public void ShowSliceStart()
        //{
        //    var slice = Instantiate(Resources.Load("Slice", typeof(GameObject)), new Vector3((float)Player.X + 2.3f, (float)Player.Y - .5f), Quaternion.Euler(0, 0, 45)) as GameObject;
        //}

        //public void ShowSliceEnd()
        //{
        //    var slice1 = Instantiate(Resources.Load("Slice", typeof(GameObject)), new Vector3((float)Player.X + 2.3f, (float)Player.Y - .2f), Quaternion.Euler(0, 0, 45)) as GameObject;
        //}

        //public void ShowSliceRightStart()
        //{
        //    var sliceRight = Instantiate(Resources.Load("SliceRight", typeof(GameObject)), new Vector3((float)Player.X + 2.7f, (float)Player.Y - .5f), Quaternion.Euler(0, 0, -45)) as GameObject;
        //}

        //public void ShowSliceRightEnd()
        //{
        //    var sliceRight = Instantiate(Resources.Load("SliceRight", typeof(GameObject)), new Vector3((float)Player.X + 2.7f, (float)Player.Y - .2f), Quaternion.Euler(0, 0, -45)) as GameObject;
        //}

        //private void SetPlayerState()
        //{
        //    var tile = GameManager.State[(9 - Player.Y) * 7 + Player.X];
        //    if (tile == 7)
        //    {
        //        GameManager.Score += 200;
        //    }
        //    else
        //    {
        //        if (tile == 6)
        //        {
        //            if (GameManager.FireLevel > 1)
        //            {
        //                GameManager.FireLevel--;
        //            }
        //        }
        //        else if (tile == 5)
        //        {
        //            GameManager.Score += 50;
        //        }

        //        GameManager.Score += 10;
        //    }

        //    GameManager.State[(9 - Player.Y) * 7 + Player.X] = 2;
        //}
    }
}
