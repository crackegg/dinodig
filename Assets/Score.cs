﻿using Assets;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    public GameManager GameManager;
    public Text Text;
    public string ScorePlaceholder = "{0}";

    public void Update()
    {
        Text.text = string.Format(ScorePlaceholder, GameManager.GameModel.Score.ToString());
    }
}
