/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID DESTROYROCK = 3769738858U;
        static const AkUniqueID DESTROYROCKEGG = 2907476757U;
        static const AkUniqueID DIGDIRT = 1550913352U;
        static const AkUniqueID DIGDIRTEGG = 2755425607U;
        static const AkUniqueID GAME_INTRO = 387455016U;
        static const AkUniqueID GAME_START = 733168346U;
        static const AkUniqueID GAMEOVER = 4158285989U;
        static const AkUniqueID HITICE = 3894462877U;
        static const AkUniqueID HITROCK = 3660205123U;
        static const AkUniqueID LEVEL_0_FIRE = 2800173371U;
        static const AkUniqueID LEVEL_1_FIRE = 4187589416U;
        static const AkUniqueID LEVEL_2_FIRE = 3931913565U;
        static const AkUniqueID LEVEL_3_FIRE = 640191250U;
        static const AkUniqueID LEVEL_4_FIRE = 1507100639U;
        static const AkUniqueID LEVEL_5_FIRE = 2308376476U;
        static const AkUniqueID LEVEL_START = 352576276U;
        static const AkUniqueID LEVEL02 = 3152323983U;
        static const AkUniqueID LEVEL03 = 3152323982U;
        static const AkUniqueID LEVEL04 = 3152323977U;
        static const AkUniqueID LEVEL05 = 3152323976U;
        static const AkUniqueID LEVEL06 = 3152323979U;
        static const AkUniqueID MENUMOVESELECTION = 3751206671U;
    } // namespace EVENTS

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID FIRELEVEL = 2458181481U;
        static const AkUniqueID SIDECHAIN_SFX = 55766211U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID DINODIGSOUNDBANK = 3269767486U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBIANCE = 2981377429U;
        static const AkUniqueID DYNOSHIT = 1383017243U;
        static const AkUniqueID FIRESYSTEM = 3393986022U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MASTER_SECONDARY_BUS = 805203703U;
        static const AkUniqueID MENU = 2607556080U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID SFX = 393239870U;
    } // namespace BUSSES

}// namespace AK

#endif // __WWISE_IDS_H__
