﻿using UnityEngine;
using System.Collections;

public class Destroy : MonoBehaviour {

    public Animator Animator;
	// Use this for initialization
	void Start () {
        Animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	    if(Animator.GetCurrentAnimatorStateInfo(0).IsName("Finished"))
        {
            Destroy(gameObject);
        }
	}
}
