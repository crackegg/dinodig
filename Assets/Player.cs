﻿using UnityEngine;

namespace Assets
{
    public class Player : MonoBehaviour
    {
        public Animator Animator;

        public ushort PosX;
        public ushort PosY;

        public Player()
        {
            PosX = 3;
            PosY = 1;
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void UpdatePosition()
        {
            transform.localPosition = new Vector2(PosX, -PosY);
        }

        public void ShowSliceStart()
        {
            var slice = Instantiate(Resources.Load("Slice", typeof(GameObject)), new Vector3(transform.position.x + 0.2f, transform.position.y - 0.2f, -5f), Quaternion.Euler(0, 0, 45)) as GameObject;
        }

        public void ShowSliceEnd()
        {
            var slice = Instantiate(Resources.Load("Slice", typeof(GameObject)), new Vector3(transform.position.x + 0.2f, transform.position.y - 0.2f, -5f), Quaternion.Euler(0, 0, 45)) as GameObject;
        }

        public void ShowSliceRightStart()
        {
            var sliceRight = Instantiate(Resources.Load("SliceRight", typeof(GameObject)), new Vector3(transform.position.x + 0.8f, transform.position.y - 0.2f, -5f), Quaternion.Euler(0, 0, -45)) as GameObject;
        }

        public void ShowSliceRightEnd()
        {
            var sliceRight = Instantiate(Resources.Load("SliceRight", typeof(GameObject)), new Vector3(transform.position.x + 0.8f, transform.position.y - 0.2f, -5f), Quaternion.Euler(0, 0, -45)) as GameObject;
        }

        public void PlayHitSound()
        {

        }
    }
}
