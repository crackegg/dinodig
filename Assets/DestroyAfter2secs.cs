﻿using UnityEngine;
using System.Collections;

public class DestroyAfter2secs : MonoBehaviour {

    private float _timeStamp;
    public SpriteRenderer r;
    public bool Grow;

	public void Start () {
        _timeStamp = Time.time + 1;
	}

    public void Update()
    {
        if(Time.time >= _timeStamp)
        {
            Destroy(gameObject);
        }

        if (r != null)
        {
            var t = r.color;
            r.color = new Color(t.r, t.g, t.b, t.a - 0.01f);
        }
        if (Grow)
        {
            transform.localScale += new Vector3(0.01f, 0.01f);
        }
    }

}
