﻿using UnityEngine;

namespace Assets
{
    public class AudioPlay : MonoBehaviour
    {
        public void Start()
        {
            var audio = GetComponent<AudioSource>();
            audio.Play();
            audio.Play(44100);
        }
    }
}
