﻿using Assets;
using UnityEngine;

public class GameState : MonoBehaviour {

    //[Inject]
    //public GameManager GameManager { get; set; }

    public GameObject Player;
    public GameObject Score;
    public Transform GameLevel;
    public GameObject Button;
    public GameObject HighScore;
    public GameObject Option;
    public AudioSource Opening;
    public AudioSource OpeningPart2;
    public GameObject Act2;
    public GameObject Act1;

    private float _timespan;
    private float _offset = 0;
    private float _maxOffset = 0.035f;
    private bool _startPressed = false;
    private bool _isInactive = false;
    private bool _flag = true;
	
	// Update is called once per frame
	public void Update () {

        if (_isInactive)
        {
            return;
        }

        if (_startPressed && transform.position.y < 15.3)
        {
            if (Time.time < _timespan)
            {
                return;
            }

            Act1.SetActive(false);
            transform.position = new Vector3(transform.position.x, transform.position.y + _offset);
            if(_offset < _maxOffset && _flag)
            {
                _offset += 0.00005f;
            }   
            else {
                _flag = false;
                _offset -= 0.00005f;
            }
        }

        if(transform.position.y > 15.3)
        {
            //GameManager.CanStartAnimation = true;
            _isInactive = true;
            Score.SetActive(true);
            Opening.Stop();
            OpeningPart2.Stop();
        }
	}

    public void Play()
    {
        GameLevel.localPosition = new Vector3(GameLevel.localPosition.x, -150);
        Button.SetActive(false);
        HighScore.SetActive(false);
        Option.SetActive(false);
        _startPressed = true;
        Act2.SetActive(true);
        OpeningPart2.Play();
        _timespan = Time.time + 1.6f;
    }
}
